import React, { Component } from "react";

// --- Importing css
import "../Style/css/Header/index.css";

// --- Importing ROutes
import Routes from "../Routes";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoggedIn: false,
        }
    }

    render(){
        return(
            <div id="header">
                <ul className="header-menu">
                    {
                        this.state.isLoggedIn ?
                            <li className="menu-option">
                                <a href={Routes.Timeline}>Timeline</a>
                            </li>
                            :
                            <li className="menu-option" />
                    }
                    {
                        this.state.isLoggedIn ?
                            <li className="menu-option">
                                <a href={Routes.MyCard}>My Cards</a>
                            </li>
                            :
                            <li className="menu-option" />
                    }
                    <li className="menu-option">
                        <a href={Routes.Design}>Design</a>
                    </li>
                    <li className="menu-option">
                        <a href={Routes.Store}>Store</a>
                    </li>
                    <li className="menu-option">
                        <a href="#">
                            {
                                this.state.isLoggedIn ?
                                    'Log out'
                                    :
                                    'Sign in'
                            }
                            
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
}

export default Index;