import React, { Component } from "react";

// --- Importing css
import "../Style/css/FileUpload/index.css";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            src:"",
            onDragOver: false,
        }
    }

    setIcon = (e) => {
        if(e){
            this.setState({ src: URL.createObjectURL(e.target.files[0]) })
            this.props.setIcon(URL.createObjectURL(e.target.files[0]));
        } else {
            this.props.setIcon('');
        }
    }

    render(){
        return(
            <div className={`fu-btn-wrapper ${ this.state.onDragOver ? 'dragover' : ''}`} 
                onDragOver={() => { this.setState({ onDragOver: true }) }}
                onDragLeave={() => { this.setState({ onDragOver: false }) }}
                >
                {
                    this.state.src ? 
                        null
                        :
                        <a href="">Icon</a>
                }
                {
                    this.state.src ? 
                        <img src={this.state.src} onClick={() => { this.setState({src: ''}, () => this.setIcon(''))}}/> 
                        : 
                        <input type="file" accept=".jpg, .jpeg, .png" onChange={(e) => { this.setIcon(e) }}/>
                }
            </div>
        )
    }
}

export default Index;