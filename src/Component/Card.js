import React, { Component } from "react";

// --- Importing css
import "../Style/css/Card/index.css";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            name: "Name",
            title: "Title",
            phoneNumber: "xxx-xxxx-xxxx",
            email: "example@example.com",
            isHoverActive: false,
        }
    }

    changeHoverState = () => {
        this.setState({
            isHoverActive: !this.state.isHoverActive
        })
    }

    isHover = () =>{
        if(this.state.isHoverActive){
            return(
                <div className="card-hover">
                    <div className="menu-option">
                        <a href="#">Edit</a>
                    </div>
                    <div className="menu-option">
                        <a href="#">Option-2</a>
                    </div>
                    <div className="menu-option">
                        <a href="#">Delete</a>
                    </div>
                </div>
            )
        }
    }

    // --- Render Icons
    renderIcons = () => {
        if( this.props.icons){
            return this.props.icons.map((item, index) => {
                if(item.url){
                    return <img src={ item.url } 
                                key={ index }
                                style={{ position: "absolute" }} 
                                width={item.size.width} 
                                height={item.size.height}/>
                }
            })    
        }
    }

    render(){
        return(
            <div className="card">
                {
                    this.renderIcons()
                }
                {
                    this.isHover()
                }
                <div className="name">{ this.props.data.name ? this.props.data.name : "name"}</div>
                <div className="title" >
                    {  this.props.data.title ? this.props.data.title : "title" }
                    { this.props.data.company ? <span className="company">{` @ ${this.props.data.company}`}</span> : ''}
                </div>
                <div className="phone-number" >{  this.props.data.phoneNumber ? this.props.data.phoneNumber : "xxx-xxxx-xxxx" }</div>
                <div className="e-mail" >{  this.props.data.email ? this.props.data.email : "example@example.com" }</div>
                {
                    this.props.disabled ?
                        null
                        :
                        <div className="menu-btn">
                            <a href="#" onClick={() => { this.changeHoverState() }}>
                                { this.state.isHoverActive ? "Close" : "Menu" }
                            </a>
                        </div>
                }
            </div>
        )
    }
}

export default Index;