import React, { Component } from 'react';

// --- Importing css
import "../../Style/css/Control/index.css";
import Slider from '@material-ui/lab/Slider';

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            option: '',
            rotation: '',
            flip: false,
            opacity: 10,
        }
    }

    selectOption = (option) => {
        this.setState({ option: option }, () => { console.log("State: ", this.state )})
    }

    updateValue = (type, value) => {
        switch(type){
            case Types.rotation:{
                this.setState({ rotation: value})
                return;
            }
            case Types.flip:{
                this.setState({ flip: value })
                return;
            }
            default: return;
        }
    }

    handleOpacity = (value) => {
        console.log("Opacity: ", value)
        this.setState({ opacity: value })
    }

    control = () => {
        switch(this.state.option){
            case Options.size:{
                return  (
                        <div className="size-control">
                            <div className="size-control-row">
                                <label>Width ( % )</label>
                                <input onChange={(e) => { this.updateValue(Types.width, e.target.value) }} />
                            </div>
                            <div className="size-control-row">
                                <label>Heigth ( % )</label>
                                <input onChange={(e) => { this.updateValue(Types.width, e.target.value) }} />
                            </div>
                        </div>
                );
            }
            case Options.rotation:{
                return  <div className="rotation-control">
                            <div className="rotation-control-row">
                                <label>Rotation</label>
                                <input onChange={(e) => { this.updateValue(Types.rotation, e.target.value) }} />
                            </div>
                            <div className="rotation-control-row">
                                <label>Flip</label>
                                <div className={`checkbox ${ this.state.flip ? 'active' : ''}`} onClick={()=>{ this.updateValue(Types.flip, !this.state.flip)}}>
                                </div>
                            </div>
                        </div>
            }
            case Options.opacity:{
                return  (
                        <div className="slider-control">
                            <Slider 
                                value={this.state.opacity}
                                min={0}
                                max={20}
                                step={1}
                                onChange={(e) => this.handleOpacity(e.target.value)}
                            />
                        </div>
                    )
            }
            default: return;
        }
    }

    render(){
        return(
            <div className="icon-control-wrapper">
                <ul className="icon-control-header">
                    <li className={`icon-control-option ${ this.state.option === Options.size ? 'active' : ''}`}><a onClick={()=>{ this.selectOption(Options.size) }}>Size</a></li>
                    <li className={`icon-control-option ${ this.state.option === Options.rotation ? 'active' : ''}`}><a onClick={()=>{ this.selectOption(Options.rotation) }}>Rotation</a></li>
                    <li className={`icon-control-option ${ this.state.option === Options.opacity ? 'active' : ''}`}><a onClick={()=>{ this.selectOption(Options.opacity) }}>Opacity</a></li>
                </ul>
                <div className="icon-control">
                    { this.control() }
                </div>
            </div>
        )
    }
}

export default Index;

const Options = {
    size: 'size',
    rotation: 'rotation',
    opacity: 'opacity'
}

const Types = {
    width: 'width',
    height: 'height',
    opacity: 'opacity',
    rotation: 'rotation',
    flip: 'flip'
}