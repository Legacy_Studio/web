import React, { Component } from "react";

// --- Importing css
import "../../Style/css/Control/index.css";

class Index extends Component{
    constructor(props){
        super(props);
        this.state={
            fontSize: '',
            fontColor: '',
            fontStyle: '',
            fontWeight: '',
        }
    }

    updateValue = (type,value) => {
        console.log("Value: ", value)
        switch(type){
            case Types.fontSize:{
                this.setState({
                    fontSize: value
                })
                return;
            }
            case Types.fontWeight:{
                this.setState({
                    fontWeight: value
                })
                return;
            }
            case Types.fontColor:{
                this.setState({
                    fontColor: value
                })
                return;
            }
            case Types.fontStyle:{
                this.setState({
                    fontStyle: value
                })
                return;
            }
            default: return;
        }
    }
    render(){
        return(
            <div className="text-control-wrapper">
                <h3 className="section-name" >{this.props.sectionName}</h3>
                <div className="text-controls">
                    <div className="text-control font-size">
                        <label>Font size</label>
                        <input className="font-styler"
                            type="numeric" 
                            placeholder="Size" 
                            onChange={(e)=>{ this.updateValue(Types.fontSize,e.target.value)}}/>
                    </div>
                    
                    <div className="text-control font-weight">
                        <label>Font Weight</label>
                        <input className="font-styler"
                            type="numeric" 
                            placeholder="Weight" 
                            onChange={(e)=>{ this.updateValue(Types.fontWeight,e.target.value)}}/>
                    </div>
                    
                    <div className="text-control font-color">
                        <label>Font Color</label>
                        <input className="font-styler"
                            type="numeric" 
                            placeholder="Color" 
                            onChange={(e)=>{ this.updateValue(Types.fontColor,e.target.value)}}/>
                    </div>
                    
                    <div className="text-control font-style">
                        <label>Font Style</label>
                        <input className="font-styler"
                            type="numeric" 
                            placeholder="Style" 
                            onChange={(e)=>{ this.updateValue(Types.fontStyle,e.target.value)}}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;

const Types = {
    fontSize: 'fontSize',
    fontWeight: 'fontWeight',
    fontStyle: 'fontStyle',
    fontColor: 'fontColor'
}

/*
    fontSize
    fontweight
    fontStyle
    color
*/