import React, { Component } from 'react';
import { Provider } from "react-redux";
import store from "./Service/Store";
import { BrowserRouter, Route, Link } from "react-router-dom";
// --- Importing Components
import Login from "./Pages/Login";
import MyCard from "./Pages/MyCard";
import Store from "./Pages/Store";
import Design from "./Pages/Design";
import Timeline from "./Pages/Timeline";

// --- Importing Routes
import Routes from "./Routes";
// --- Importing App css
import "./App.css";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div id="app-wrapper">
          <Route path={"/"} component={Login} exact />
          <Route path={`/timeline`} component={ Timeline } />
          <Route path={`/Store`} component={ Store } />
          <Route path={`/${Routes.MyCard}`} component={ MyCard } exact />
          <Route path={`/${Routes.Design}`} component={ Design } exact />
          <Route path={`/Card/:id`} component={ Design } />
        </div>
      </BrowserRouter>
    );
  }
}



export default () => {
  return(
    <Provider store={ store }>
      <App />
    </Provider>
  )
};
