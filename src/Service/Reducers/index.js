import { combineReducers } from "redux";

// --- Importing reducers
import MatchMaking from "./MatchMaking";
// --- Combining reducers

export default combineReducers({
    MatchMaking: MatchMaking,
})