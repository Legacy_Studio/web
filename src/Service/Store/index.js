import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

import Reducers from "../Reducers";

const middleware = applyMiddleware(thunk, logger);
export default createStore(Reducers, middleware);
