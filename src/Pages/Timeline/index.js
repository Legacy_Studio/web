import React, { Component } from "react";

// --- Importing Components
import Header from "../../Component/Header";
import Card from "../../Component/Card";

// --- Importing css
import "../../Style/css/Timeline/index.css";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }

    renderCard = (index, item) => {
        return(
            <li className="card-wrapper">
                {
                    item == list[index-1] ? null : <h3 className="date">{item.date}</h3>
                }
                <Card 
                    data={item}
                />
            </li>
        )
    }

    render(){
        return(
            <div id="timeline-wrapper">
                <Header />
                <div id="timeline">
                    <ul id="history-wrapper">
                        {
                            list.map((item, index) => {
                                return this.renderCard(index, item)
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

export default Index;

const list = [
    {
        name:"Nomio",
        title: "Software Engineer",
        company: "My card",
        phoneNumber:"080-1982-5003",
        email: "nomikeeper@gmail.com",
        date: "2019-03-02"
    },{
        name:"Dimitri",
        title: "CEO",
        company: "Morphius",
        phoneNumber:"xxx-xxxx-xxxx",
        email: "dimitri@gmail.com",
        date: "2019-02-25"
    },{
        name:"Mendbayar",
        title: "CFO",
        company: "My card",
        phoneNumber:"xxx-xxxx-xxxx",
        email: "mendee@gmail.com",
        date: "2019-01-12"
    },{
        name:"David Cooper",
        title: "Marketing Manager",
        company: "Bee Hive",
        phoneNumber:"xxx-xxxx-xxxx",
        email: "davidcooper@gmail.com",
        date: "2018-12-15"
    },{
        name:"Edward School",
        title: "Lead Software Engineer",
        company: "Lab One",
        phoneNumber:"xxx-xxxx-xxxx",
        email: "edschool@gmail.com",
        date: "2018-11-22"
    },
];

