import React, { Component } from "react";
import _ from "../../Style/css/Login/index.css";
import Footer from "../../Component/Footer";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
        }
    }

    changeField = ( type, value ) => {
        switch(type){
            case "username":
                this.setState({ username: value  })
                return;
            case "password":
                this.setState({ password: value})
                return;
            default: return;
        }
    }

    login = () => {
        console.log("Credentials: ", this.state);
        console.log("Login!")
    }

    render(){
        return(
            <div id="content">
                <div className="form-wrapper">
                    <form id="form">
                        <h1> Login </h1>
                        <div className="input-field">
                            <label>Username</label>
                            <input 
                                type="text" 
                                placeholder="Username..." 
                                name="username" 
                                onChange={(e) => { this.changeField("username", e.target.value) }}
                                />
                        </div>
                        <div className="input-field">
                            <label>Password</label>
                            <input 
                                type="password" 
                                placeholder="Username..." 
                                name="username"
                                onChange={(e) => { this.changeField("password", e.target.value) }}
                                />
                        </div>
                        <a href="#" onClick={() => { this.login() }}>Submit </a>
                    </form>
                </div>
                <Footer />
            </div>
        )
    }
}

export default Index;