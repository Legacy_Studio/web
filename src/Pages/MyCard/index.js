import React, { Component } from "react";

// --- Importing Components
import Header from "../../Component/Header";
import Footer from "../../Component/Footer";
import Card from "../../Component/Card";
// --- Importing Css
import "../../Style/css/MyCard/index.css";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            myCards:MyCards,
        }
    }

    renderMyCards(){
        if(this.state.myCards && this.state.myCards.length > 0){
            return this.state.myCards.map((item,index) => {
                return(
                    <li key={index +1 }
                        className="card-row">
                        <div className="state-wrapper">
                            <p className={`state ${ index == 0 ? 'active' : ''}`}>
                                { index == 0 ? 'Live' : 'Design' }
                            </p>
                        </div>
                        <div className="card-wrapper">
                            <Card 
                                data={item}
                                disabled={true}
                            />
                        </div>
                        <div className="controls-wrapper">
                            <div className="option">
                                <a>Edit</a>
                            </div>
                            <div className="option delete">
                                <a>Delete</a>
                            </div>
                        </div>
                    </li>
                )
            })
        } else {
            return(<li>Currently There is no Card.</li>);
        }
    }

    render(){
        return(
            <div>
                <Header />
                <div id="my-card">
                    <ul id="cards-wrapper">
                        <li key={0}
                            className="card-row">
                            <div className="state-header">
                                <h3>State</h3>
                            </div>
                            <div className="card-header">
                                <h3>Card</h3>
                            </div>
                            <div className="controls-header">
                                <h3>Options</h3>
                            </div>
                        </li>
                        { this.renderMyCards() }
                    </ul>
                </div>
                <Footer />
            </div>
        );
    }
}

export default Index;

const MyCards = [
    {
        name:"Nomio",
        title: "CEO & CTO",
        company: "My card",
        phoneNumber:"080-1982-5003",
        email: "nomikeeper@gmail.com",
        date: "2019-03-02"
    },
    {
        name:"Nomio",
        title: "Singer / Songwriter",
        company: "",
        phoneNumber:"080-1982-5003",
        email: "nomikeeper@gmail.com",
        date: "2019-03-02"
    },
    {
        name:"Nomio",
        title: "Advisor",
        company: "Morphius",
        phoneNumber:"080-1982-5003",
        email: "nomikeeper@gmail.com",
        date: "2019-03-02"
    }
]