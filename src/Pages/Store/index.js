import React, { Component } from "react";

// --- Importing Components
import Header from "../../Component/Header";
import Footer from "../../Component/Footer";
import Card from "../../Component/Card";
import FuBtn from "../../Component/Fileupload";
import IconControl from "../../Component/Control/icon";

import "../../Style/css/Store/index.css";

class Index extends Component{
    constructor(props){
        super(props);
        this.state={
            keyword: ''
        }
    }

    updateKeyWord = (value) => {
        this.setState({ keyword: value })
    }

    render(){
        return(
            <div>
                <Header />
                <div id="content-wrapper">
                    <div id="filter-container">
                        <div className="filter-column">
                            <label>Type</label>
                            <select>
                                <option value="Halven">Halven</option>
                                <option value="One piece">One piece</option>
                                <option value="Spinner">Spinner</option>
                            </select>
                        </div>
                        <div className="filter-column">
                            <label>Industry</label>
                            <select>
                                <option value="Halven">Entertainment</option>
                                <option value="One piece">Business</option>
                                <option value="Spinner">Individual</option>
                            </select>
                        </div>
                        <div className="filter-column">
                            <label>Keyword</label>
                            <input 
                                type={"text"}
                                onChange={(e)=>{ this.updateKeyWord(e.target.value) }}
                            />
                        </div>
                        <div id="button-container" onClick={()=>{ alert("Set filter")}}>
                            <a className="set-button">Set</a>                        
                        </div>
                    </div>
                    <Card />
                </div>
                <Footer />
            </div>
        )
    }
}

export default Index;