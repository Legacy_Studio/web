import React, { Component } from "react";

// --- Importing Components
import Header from "../../Component/Header";
import Footer from "../../Component/Footer";
import Card from "../../Component/Card";
import FuBtn from "../../Component/Fileupload";
import IconControl from "../../Component/Control/icon";
import TextControl from "../../Component/Control/text";

// --- Importinc css
import "../../Style/css/Design/index.css";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            name: '',
            title: '',
            company: '',
            phoneNumber: '',
            email: '',
            icons: Icons,
            activeSection: '',
            activeSectionName: '',
            activeIconIndex: '',
            activeTextIndex: '',
        }
    }

    // --- Update Field
    changeField = (type, value) => {
        switch(type){
            case types.name:{
                this.setState({ name: value })
                return;
            }
            case types.email:{
                this.setState({ email: value})
                return;
            }
            case types.phoneNumber:{
                this.setState({ phoneNumber: value })
                return;
            }
            case types.title:{
                this.setState({ title: value });
                return;
            }
            case types.company:{
                this.setState({ company: value });
                return;
            }
            default: return;
        }
    }

    // --- Set Icon
    setIcon = (URL, index, zIndex) => {
        let icons = this.state.icons;
        let UpdatingIcons = icons[index];
        UpdatingIcons = {
            url: URL,
            position: null,
            zIndex: zIndex ? 3 + zIndex : 3,
            size: icons[index].size
        }
        icons[index] = UpdatingIcons;
        this.setState({ icons: icons})
    }

    iconControl = () => {
        return(
            <div className="icon-control-wrapper">
                <div className="icon-control-header"></div>
                <div className="icon-control"></div>
            </div>
        )
    }

    control = () => {
        switch(this.state.activeSection){
            case activeSection.text:{
                return <TextControl 
                            sectionName={this.state.activeSectionName}
                        />;
            }
            case activeSection.icon:{
                return <IconControl />
            }
            default: return <h2>Control</h2>;
        }
    }

    // --- Enable/Disable Icon field to modify it's settings
    selectIconField = (index) => {
        this.setState({ 
            activeIconIndex: index,
            activeSection: activeSection.icon,
            activeTextIndex: '', 
            activeSectionName: ''
        })
    }

    // --- Enable/Disable Text field to modify it's settings
    selectTextField = (index, name) => {
        this.setState({ 
            activeTextIndex: index,
            activeSection: activeSection.text,
            activeIconIndex: '', 
            activeSectionName: name
        })
    }

    deselectControl = () => {
        this.setState({ 
            activeIconIndex: '',
            activeTextIndex: '',
            activeSection: '',
            activeSectionName: ''
        })
    }

    // Save the business card into inventory
    saveCard = () => {
        alert("Save the card!");
    }

    // Reset Card infos
    resertCardInfo = () => {
        alert("Reset Card Info");
    }

    // Main render function
    render(){
        return(
            <div>
                <Header />
                <div id="design">
                    <div id="card-wrapper">
                        <Card 
                            data={{
                                name: this.state.name, 
                                title:this.state.title ,
                                company:this.state.company ,
                                phoneNumber: this.state.phoneNumber ,
                                email: this.state.email 
                            }}
                            icons={ this.state.icons }
                            disabled={true}
                        />
                        <div className="reset option-btn">
                            <a onClick={()=>{ this.resertCardInfo() }}>Reset</a>
                        </div>
                        <div className="save option-btn">
                            <a onClick={()=>{ this.saveCard() }}>Save</a>
                        </div>
                    </div>
                    <div id="info-wrapper">
                        <div className="subtitle">
                            <h3>Basic info</h3>
                        </div>
                        <div className="input-section">
                            <div className={`input-field ${ this.state.activeTextIndex === 0 ? 'active' : ''}`}
                                onClick={() => { this.selectTextField(0, 'Name') }}>
                                <label>Name</label>
                                <input 
                                    type="text" 
                                    placeholder="Name" 
                                    name="name"
                                    onChange={(e) => { this.changeField(types.name, e.target.value) }}
                                    />
                            </div>
                            
                            <div className={`input-field ${ this.state.activeTextIndex === 1 ? 'active' : ''}`}
                                onClick={() => { this.selectTextField(1, 'Title') }}>
                                <label>Title</label>
                                <input 
                                    type="text" 
                                    placeholder="Title" 
                                    name="title"
                                    onChange={(e) => { this.changeField(types.title, e.target.value) }}
                                    />
                            </div>

                            <div className={`input-field ${ this.state.activeTextIndex === 2 ? 'active' : ''}`}
                                onClick={() => { this.selectTextField(2, 'Company') }}>
                                <label>Company</label>
                                <input 
                                    type="text" 
                                    placeholder="Company" 
                                    name="company"
                                    onChange={(e) => { this.changeField(types.company, e.target.value) }}
                                    />
                            </div>
                            
                            <div className={`input-field ${ this.state.activeTextIndex === 3 ? 'active' : ''}`}
                                onClick={() => { this.selectTextField(3, 'Phone number') }}>
                                <label>Phone number</label>
                                <input 
                                    type="number" 
                                    placeholder="" 
                                    name="phonenumber"
                                    onChange={(e) => { this.changeField(types.phoneNumber, e.target.value) }}
                                    />
                            </div>
                            
                            <div className={`input-field ${ this.state.activeTextIndex === 4 ? 'active' : ''}`}
                                onClick={() => { this.selectTextField(4, 'E-mail') }}>
                                <label>E-mail</label>
                                <input 
                                    type="text" 
                                    placeholder="E-mail" 
                                    name="e-mail"
                                    onChange={(e) => { this.changeField(types.email, e.target.value) }}
                                    />
                            </div>
                        </div>
                    </div>
                    <div id="images-wrapper">
                        <div className={`image-btn ${ this.state.activeIconIndex === 0 ? 'active' : '' }`} 
                            onClick={() => { this.selectIconField(0) }}
                            >
                            <FuBtn setIcon={(URL)=>{ this.setIcon(URL, 0)}} zIndex={ this.state.icons[0].zIndex } />
                        </div>
                        <div className={`image-btn ${ this.state.activeIconIndex === 1 ? 'active' : '' }`} 
                            onClick={() => { this.selectIconField(1) }}
                            >
                            <FuBtn setIcon={(URL)=>{ this.setIcon(URL, 1)}} zIndex={ this.state.icons[1].zIndex } />
                        </div>
                        <div className={`image-btn ${ this.state.activeIconIndex === 2 ? 'active' : '' }`} 
                            onClick={() => { this.selectIconField(2) }}
                            >
                            <FuBtn setIcon={(URL)=>{ this.setIcon(URL, 2)}} zIndex={ this.state.icons[2].zIndex } />
                        </div>
                        <div className={`image-btn ${ this.state.activeIconIndex === 3 ? 'active' : '' }`} 
                            onClick={() => { this.selectIconField(3) }}
                            >
                            <FuBtn setIcon={(URL)=>{ this.setIcon(URL, 3)}} zIndex={ this.state.icons[3].zIndex } />
                        </div>
                        <div className={`image-btn ${ this.state.activeIconIndex === 4 ? 'active' : '' }`} 
                            onClick={() => { this.selectIconField(4) }}
                            >
                            <FuBtn setIcon={(URL)=>{ this.setIcon(URL, 4)}} zIndex={ this.state.icons[4].zIndex } />
                        </div>
                        <div className={`image-btn ${ this.state.activeIconIndex === 5 ? 'active' : '' }`} 
                            onClick={() => { this.selectIconField(5) }}
                            >
                            <FuBtn setIcon={(URL)=>{ this.setIcon(URL, 5)}} zIndex={ this.state.icons[5].zIndex } />
                        </div>
                    </div>
                    <div id="control-wrapper">
                        <div id="control">
                            { this.control() }
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default Index;

const types = {
    name: 'name',
    title: 'title',
    company: 'company',
    phoneNumber: 'phoneNumber',
    email: 'email'
}

const activeSection = {
    text: 'text',
    icon: 'icon'
}
const Icons = [
    {
        url: '',
        position: null,
        zIndex: 3,
        size:{
            height: 100,
            width: 100,
        }
    },
    {
        url: '',
        position: null,
        zIndex: 3,
        size:{
            height: 100,
            width: 100,
        }
    },
    {
        url: '',
        position: null,
        zIndex: 3,
        size:{
            height: 100,
            width: 100,
        }
    },
    {
        url: '',
        position: null,
        zIndex: 3,
        size:{
            height: 100,
            width: 100,
        }
    },
    {
        url: '',
        position: null,
        zIndex: 3,
        size:{
            height: 100,
            width: 100,
        }
    },
    {
        url: '',
        position: null,
        zIndex: 3,
        size:{
            height: 100,
            width: 100,
        }
    },
]